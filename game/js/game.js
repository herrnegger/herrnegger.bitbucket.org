(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

//global variables
window.onload = function () {
  var game = new Phaser.Game(360, 640, Phaser.AUTO, 'recycler');

  // Game States
  game.state.add('boot', require('./states/boot'));
  game.state.add('gameover', require('./states/gameover'));
  game.state.add('menu', require('./states/menu'));
  game.state.add('play', require('./states/play'));
  game.state.add('preload', require('./states/preload'));
  

  game.state.start('boot');
};
},{"./states/boot":6,"./states/gameover":7,"./states/menu":8,"./states/play":9,"./states/preload":10}],2:[function(require,module,exports){
'use strict';

var Bin = function(game, x, y, binName) {
  this.binType = binName;
  this.currentBin = this.binConfig[binName];
  console.log(this.currentBin);
  Phaser.Sprite.call(this, game, x, y, 'bins', this.currentBin.frame);
  // initialize your prefab here

  // enable physics on the ground sprite
  // this is needed for collision detection
  this.game.physics.arcade.enableBody(this);

  // we don't want the ground's body
  // to be affected by gravity
  this.body.allowGravity = false;
  this.body.immovable = true;

  this.body.setSize(this.width*0.8, this.height*0.5, this.width*0.1, this.height*0.5);
};

Bin.prototype = Object.create(Phaser.Sprite.prototype);
Bin.prototype.constructor = Bin;

Bin.prototype.binConfig = {
  'metal': {
    name: 'Metall',
    frame: 0
  },
  'paper': {
    name: 'Altpapier',
    frame: 1
  },
  'plasticbottles': {
    name: 'Plastikflaschen',
    frame: 2
  },
  'residual': {
    name: 'Restmüll',
    frame: 3
  },
  'bio': {
    name: 'Biomüll',
    frame: 4
  },
  'whiteglass': {
    name: 'Weißglas',
    frame: 5
  },
  'coloredglass': {
    name: 'Buntglas',
    frame: 6
  }
}


Bin.prototype.update = function() {

  // write your prefab's specific update code here

};

module.exports = Bin;

},{}],3:[function(require,module,exports){
'use strict';

var Bin = require('../prefabs/bin');


var BinGroup = function(game, parent, binsToAdd) {
  Phaser.Group.call(this, game, parent);
  var _this = this;

  this.bins = [];
  this.width = 0;

  // add bins to group
  var margin = 5;
  var countBins = binsToAdd.length;

  for (var index = 0; index < countBins; ++index) {
    var binName = binsToAdd[index];
    var currentBin = new Bin(_this.game, 0, 0, binName);
    _this.add(currentBin);
    _this.bins.push(currentBin);
    currentBin.x = (currentBin.width + margin) * index;
    this.width += currentBin.width + margin;
  }

  // initialize your prefab here

};

BinGroup.prototype = Object.create(Phaser.Group.prototype);
BinGroup.prototype.constructor = BinGroup;

BinGroup.prototype.update = function() {

  // write your prefab's specific update code here

};

module.exports = BinGroup;

},{"../prefabs/bin":2}],4:[function(require,module,exports){
'use strict';

var Ground = function(game, x, y, width, height) {
  Phaser.TileSprite.call(this, game, x, y, width, height, 'ground');

  // enable physics on the ground sprite
  // this is needed for collision detection
  this.game.physics.arcade.enableBody(this);

  // we don't want the ground's body
  // to be affected by gravity
  this.body.allowGravity = false;
  this.body.immovable = true;
  // initialize your prefab here

  this.body.setSize(this.width*0.8, this.height*0.9, this.width*0.1, this.height*0.1);
};

Ground.prototype = Object.create(Phaser.TileSprite.prototype);
Ground.prototype.constructor = Ground;

Ground.prototype.update = function() {

  // write your prefab's specific update code here

};

module.exports = Ground;

},{}],5:[function(require,module,exports){
'use strict';

var Item = function(game, x, y, binType) {
  this.binType = binType;
  this.currentItem = _.sample(this.itemConfig[binType]);

  Phaser.Sprite.call(this, game, x, y, 'items', this.currentItem.frame);

  this.anchor.setTo(0.5, 0.5);

  this.game.physics.arcade.enableBody(this);
  this.body.collideWorldBounds = true;

  this.inputEnabled = true;
  this.input.enableDrag();
  this.input.allowVerticalDrag = false;

  this.onGround = false;
  this.alive = true;

  this.body.setSize(this.width*0.8, this.height*0.8, this.width*0.1, this.height*0.1);
  // initialize your prefab here

};

Item.prototype = Object.create(Phaser.Sprite.prototype);
Item.prototype.constructor = Item;

Item.prototype.itemConfig = {
  paper: [
    {
      name: "Papierstern",
      frame: 1
    }
  ],
  bio: [
    {
      name: "Banane",
      frame: 0
    },
    {
      name: "Apfel",
      frame: 2
    }
  ],
  coloredglass: [
    {
      name: "Buntglasflasche",
      frame: 3
    }
  ],
  whiteglass: [
    {
      name: "Weißglasflasche",
      frame: 4
    }
  ]
};

Item.prototype.update = function() {

  // write your prefab's specific update code here

};


module.exports = Item;

},{}],6:[function(require,module,exports){

'use strict';

function Boot() {
}

Boot.prototype = {
  preload: function() {
    this.load.image('preloader', 'assets/preloader.gif');
  },
  create: function() {

    this.game.input.maxPointers = 1;
    this.game.state.start('preload');
  }
};

module.exports = Boot;

},{}],7:[function(require,module,exports){
'use strict';

var Ground = require('../prefabs/ground');
var BinGroup = require('../prefabs/binGroup');

function GameOver() {}

GameOver.prototype = {
  preload: function() {

  },
  create: function() {
    this.background = this.game.add.sprite(0,0,'background');

    this.highscoreBackground = this.game.add.sprite(this.game.width/2,0,'highscore_background');
    this.highscoreBackground.anchor.setTo(0.5,0);

    this.highscoreText = this.game.add.text(this.game.width/2,170, "Trennmeister!\nDu hast\n"+this.game.points+"\nPunkte erreicht!");
    this.highscoreText.anchor.setTo(0.5);

    this.highscoreText.font = 'Boogaloo';
    this.highscoreText.fontSize = 26;
    this.highscoreText.align = 'center';

    this.ground = new Ground(this.game, 0, 400, 360, 245);
    this.game.add.existing(this.ground);

    this.bins = this.game.add.group();
    this.binGroup = new BinGroup(this.game, this.bins, ['metal', 'paper', 'residual', 'bio', 'whiteglass']);
    this.game.add.existing(this.binGroup);


    this.binScaling = 0.75;
    this.binGroup.y = 280;
    this.binGroup.scale = new Phaser.Point(this.binScaling, this.binScaling);
    this.binGroup.x = (this.game.width - this.binGroup.width*this.binScaling) /2;

    this.startButton = this.game.add.button(this.game.width/4, 540, 'btn_start', this.startClick, this);
    this.startButton.anchor.setTo(0.5,0.5);

    this.highscoreButton = this.game.add.button((this.game.width/4)*3, 540, 'btn_highscore', this.highscoreClick, this);
    this.highscoreButton.anchor.setTo(0.5,0.5);
  },
  startClick: function() {
    // start button click handler
    // start the 'play' state
    this.game.state.start('play');
  },
  highscoreClick: function() {
    // start button click handler
    // start the 'play' state
    // this.game.state.start('play');
  },
};

module.exports = GameOver;

},{"../prefabs/binGroup":3,"../prefabs/ground":4}],8:[function(require,module,exports){
'use strict';

var Ground = require('../prefabs/ground');
var BinGroup = require('../prefabs/binGroup');

function Menu() {}

Menu.prototype = {
  preload: function() {

  },
  create: function() {
    this.background = this.game.add.sprite(0,0,'background');
    this.ground = new Ground(this.game, 0, 400, 360, 245);
    this.game.add.existing(this.ground);

    this.bins = this.game.add.group();
    this.binGroup = new BinGroup(this.game, this.bins, ['metal', 'paper', 'residual', 'bio', 'whiteglass']);
    this.game.add.existing(this.binGroup);


    this.binScaling = 0.75;
    this.binGroup.y = 280;
    this.binGroup.scale = new Phaser.Point(this.binScaling, this.binScaling);
    this.binGroup.x = (this.game.width - this.binGroup.width*this.binScaling) /2;

    this.startButton = this.game.add.button(this.game.width/4, 540, 'btn_start', this.startClick, this);
    this.startButton.anchor.setTo(0.5,0.5);

    this.highscoreButton = this.game.add.button((this.game.width/4)*3, 540, 'btn_highscore', this.highscoreClick, this);
    this.highscoreButton.anchor.setTo(0.5,0.5);
  },
  startClick: function() {
    // start button click handler
    // start the 'play' state
    this.game.state.start('play');
  },
  highscoreClick: function() {
    // start button click handler
    // start the 'play' state
    // this.game.state.start('play');
  },
};

module.exports = Menu;

},{"../prefabs/binGroup":3,"../prefabs/ground":4}],9:[function(require,module,exports){
'use strict';

  var Ground = require('../prefabs/ground');
  var BinGroup = require('../prefabs/binGroup');
  var Bin = require('../prefabs/bin');
  var Item = require('../prefabs/item');

  function Play() {}
  Play.prototype = {
    create: function() {

      this.game.points = 0;
      this.lives = 3;

      // start the phaser arcade physics engine
      this.game.physics.startSystem(Phaser.Physics.ARCADE);
      this.game.physics.arcade.gravity.y = 150;

      this.background = this.game.add.sprite(0,0,'background');
      this.ground = new Ground(this.game, 0, 550, 360, 245);
      this.game.add.existing(this.ground);

      // create and add a group to hold our item prefabs
      this.items = this.game.add.group();

      this.currentLevel = 0;

      this.generateBins();

      this.itemGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 3, this.generateItem, this);
      this.itemGenerator.timer.start();


      this.pointsText = this.game.add.text(24,24, "0");
      this.pointsText.anchor.setTo(0.5);

      this.pointsText.font = 'Boogaloo';
      this.pointsText.fontSize = 30;


      this.heartsGroup = this.game.add.group();
      this.hearts = [];

      for(var i = 0; i<3; ++i) {
        var heart = this.game.add.sprite(i*33,0,'heart');
        this.heartsGroup.add(heart);
        this.hearts.push(heart);
      }

      this.heartsGroup.x = 250;
      this.heartsGroup.y = 10;

    },
    generateBins: function () {
      this.bins = this.game.add.group();
      this.binGroup = new BinGroup(this.game, this.bins, this.getValidBins());
      this.game.add.existing(this.binGroup);

      this.binScaling = 0.75;
      this.binGroup.y = 430;
      this.binGroup.scale = new Phaser.Point(this.binScaling, this.binScaling);
      this.binGroup.x = (this.game.width - this.binGroup.width*this.binScaling) /2;
    },
    update: function() {
      // enable collisions between the bird and the ground
      //this.game.physics.arcade.collide(this.bird, this.ground, this.collisionHandler, null, this);
      this.items.forEach(function(item) {
          //this.checkScore(pipeGroup);
          if(item.alive) {
            this.game.physics.arcade.collide(item, this.ground, this.collisionHandler, null, this);

            this.binGroup.bins.forEach(function(bin) {
              this.game.physics.arcade.collide(item, bin, this.collisionHandler, null, this);
            }, this);
          }
          else {
            //console.log('not alive');
          }
      }, this);

      //if(!this.gameover) {
          // enable collisions between the bird and each group in the items group
          /*this.items.forEach(function(item) {
              this.checkScore(pipeGroup);
              this.game.physics.arcade.collide(this.bird, item, this.collisionHandler, null, this);
          }, this);*/
      //}
    },
    collisionHandler: function(item, hitty) {
      item.alive = false;
      item.body.allowGravity = false;

      if(hitty instanceof Ground && !item.onGround) {
          //this.groundHitSound.play();
          //this.scoreboard = new Scoreboard(this.game);
          //this.game.add.existing(this.scoreboard);
          //this.scoreboard.show(this.score);
          item.onGround = true;
          this.lives = this.lives - 1;
          item.destroy();
      } else if (hitty instanceof Bin){
          //this.pipeHitSound.play();
          if(item.binType == hitty.binType) {
            this.game.points = this.game.points + 1;
          }
          else {
            this.lives = this.lives - 1;
          }
          item.destroy();
      }

      this.pointsText.text = this.game.points + "";
      this.setLives(this.lives);

      if(this.lives < 0) {
        this.game.state.start('gameover');
      }

      // if(!this.gameover) {
      //     this.gameover = true;
      //     this.bird.kill();
      //     this.pipes.callAll('stop');
      //     this.pipeGenerator.timer.stop();
      //     this.ground.stopScroll();
      // }
    },
    generateItem: function () {
      var itemX = this.game.rnd.integerInRange(50, this.game.width-50);
      var item = this.items.getFirstExists(false);
      if(!item) {
          item = new Item(this.game, itemX, -80, _.sample(this.getValidBins()));
          this.items.add(item);
      }
      //itemGroup.reset(20, itemX);
    },
    setLives: function (lives) {
      this.heartsGroup.setAll('alpha', 1);
      if(lives <= 2) {
        this.hearts[0].alpha = 0.3;
      }
      if(lives <= 1) {
        this.hearts[1].alpha = 0.3;
      }
      if(lives <= 0) {
        this.hearts[2].alpha = 0.3;
      }
    },
    getValidBins: function () {
      return this.levels[this.currentLevel].bins;
    },
    levels: [
      { // Level 1
        bins: ['paper', 'bio'],

      },
      { // Level 2
        bins: ['paper', 'bio', 'whiteglass']
      }
    ]
  };

  module.exports = Play;

},{"../prefabs/bin":2,"../prefabs/binGroup":3,"../prefabs/ground":4,"../prefabs/item":5}],10:[function(require,module,exports){

'use strict';
function Preload() {
  this.asset = null;
  this.ready = false;
}

Preload.prototype = {
  preload: function() {
    this.asset = this.add.sprite(this.width/2,this.height/2, 'preloader');
    this.asset.anchor.setTo(0.5, 0.5);

    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
    this.load.setPreloadSprite(this.asset);

    this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

    this.load.image('background', 'assets/img/small/background.png');
    this.load.image('ground', 'assets/img/small/ground.png');
    this.load.image('heart', 'assets/img/small/heart.png');
    this.load.image('btn_highscore', 'assets/img/small/btn_highscore.png');
    this.load.image('btn_start', 'assets/img/small/btn_start.png');
    this.load.image('highscore_background', 'assets/img/small/highscore_background.png');

    this.load.spritesheet('bins', 'assets/img/small/bins.png', 85, 218);
    this.load.spritesheet('items', 'assets/img/small/items.png', 54, 54);
  },
  create: function() {
    this.asset.cropEnabled = false;
  },
  update: function() {
    if(!!this.ready) {
      this.game.state.start('gameover');
    }
  },
  onLoadComplete: function() {
    this.ready = true;
  }
};

module.exports = Preload;

},{}]},{},[1])